//
//  AccoutsCell.swift
//  PFFantasy-Admin
//
//  Created by Mensah Shadrach on 2/16/18.
//  Copyright © 2018 Mensah Shadrach. All rights reserved.
//

import Cocoa

class AccoutsCell: NSTableCellView {

    @IBOutlet weak var amount: NSTextField!
    @IBOutlet weak var username: NSTextField!
    override func draw(_ dirtyRect: NSRect) {
        super.draw(dirtyRect)

        // Drawing code here.
    }
    
}
